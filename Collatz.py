#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------

#instantiate a global dictionary
cache = dict()


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    # add min and max
    return [int(a[0]), int(a[1])]
    # return [a[0], a[1]]                                                # add min and max

    # a = [int(x) for x in s.split()]
    # return [min(a), max(a)]
# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert isinstance(i, int)
    assert isinstance(j, int)

    # ensures that the function can take in larger number first
    if i > j:
        tmp = i
        i = j
        j = tmp
        assert(i <= j)

    # takes care of repeated numbers
    if i == j:
        return helper(i, 1, cache)

    max_path = 0
    cache[0] = 0

    # checks all the numbers in a range and stores it in a file
    for x in range(i, j + 1):
        cache[x] = helper(x, 1, cache)
        if cache[x] > max_path:
            max_path = cache[x]

    assert isinstance(max_path, int)
    return max_path

# returns the path length of a number and stores it in the cache if it does not already exist
def helper(n, count, cache):
    # using the cache if the number exists
    if n in cache:
        return cache[n] + count - 1

    if(n == 1):
        return count

    elif(n % 2 == 0):
        # adds the number to the cache if it does not exist yet
        num = helper(n // 2, 1, cache) + 1
        cache[n] = num

    else:
        return helper(3 * n + 1, 1, cache) + 1
        num = helper(3 * n + 1, 1, cache) + 1
        cache[n] = num

    return cache[n]


# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
