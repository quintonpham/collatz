import sys
# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------
mem = dict()
def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]                                                # add min and max
    # a = [int(x) for x in s.split()]
    # return [min(a), max(a)]
# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # fixing the dev pushing to master error
#----------------------------------------------------- 

    #----------------------------------------------------- 
    assert isinstance(i,int)
    assert isinstance(j,int)

    if i > j:
        tmp = i
        i = j
        j = tmp
        assert(i <= j)

    if i == j:
        return helper(i)
   
    max_path = 0
    # for y in range(i-1, 0, -1):
    #     mem[y] = helper(y, 1, mem)
    mem[0] = 0

    for x in range(i, j + 1):
        mem[x] = helper(x, 1, mem)
    # print(mem)
        # print(mem)
        if mem[x] > max_path:
            max_path = mem[x]
    # print(mem)
    return max_path
    # print(mem)
    # return max(mem.values())
#-----------------------------------------------------
    # return 1    

def helper (n, count = 1, mem = {}):
    if n in mem:
        return mem[n] + count - 1
    

    # print(n)
    # print(mem)
    
    if( n == 1 ):
        return count
    elif( n % 2 == 0 ):
        num = helper ( n // 2   , 1, mem) + 1
        mem[n] = num
    else:
        num = helper ( 3 * n + 1, 1, mem) + 1
        mem[n] = num
    return mem[n]
    # else:
    #     counter += 1
    #     n = n * 3 + 1

    # return max(x, counter)


# -------------
# collatz_print
# -------------



def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        if not s.strip() :
            continue
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)

if __name__ == "__main__":
    collatz_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
% cat RunCollatz.in
1 10
100 200
201 210
900 1000



% python RunCollatz.py < RunCollatz.in > RunCollatz.out
(on Windows PowerShell, the command is as follows:
"c:\> Get-Content RunCollatz.in | py RunCollatz.py > RunCollatz.out"
)


% cat RunCollatz.out
1 10 1
100 200 1
201 210 1
900 1000 1



% pydoc3 -w Collatz
(Note: on Windows PowerShell, the command is as follows:
"c:\> python -m pydoc -w Collatz"
)
# That creates the file Collatz.html
"""
#!/usr/bin/env python3


# r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
# w = StringIO()
# collatz_solve(r, w)

# import time

# start = time.time()
# collatz_eval(1, 1000000)
# end = time.time()
# print(end - start)